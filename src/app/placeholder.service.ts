import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class PlaceholderService 
{
  apiURL:string = 'https://swapi.co/api/';

  constructor(private http:HttpClient) { }

  // Declare functions of the service
  getAllData(category:string)
  {
    return this.http.get(`${this.apiURL}${category}`)
                    .pipe(catchError(this.handleError<Object[]>('getAllData', [])));
  }

  getSingleData(category:string, id:number)
  {
    return this.http.get(`${this.apiURL}${category}/${id}`)
                    .pipe(catchError(this.handleError<Object[]>('getSingleData', [])));
  }

  private handleError<T> (operation = 'operation', result?: T)
  {
    return (error: any): Observable<T> => 
    {
      console.error(error);
      return throwError('Error: ' + error);
    };
  }
}
