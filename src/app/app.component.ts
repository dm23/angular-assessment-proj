import { Component, } from '@angular/core';
import { PlaceholderService } from './placeholder.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent
{
  constructor(private service:PlaceholderService) { }

  // Declaring models for component
  notFound:boolean = false;
  dataSet;
  itemID:number = 1;
  category:string = 'people';
  optionsList:Object[] = [{opt:'people'}, 
                          {opt:'planets'}, 
                          {opt:'species'}, 
                          {opt:'starships'}, 
                          {opt:'vehicles'}];

  // Declaring functions for the component
  // Calling a method from the service
  startService()
  {
    this.service.getAllData("people")
                .subscribe( (result)=>{this.dataSet = result; console.log(result)} );
  }

  handleClickEvent()
  {
    // Invoke service, passing parameters
    this.service.getSingleData(this.category, this.itemID)
                .subscribe( (result)=>{ this.notFound = false;
                                        this.dataSet = result;
                                        console.log(result) }, 
                             error=>{ this.notFound = true });
  }
}
